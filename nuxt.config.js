module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'slovo',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Народный англо-русский словарь компьтерных терминов' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  router: {
    extendRoutes (routes, resolve) {
      routes.push({
        name: 'все слова',
        path: encodeURI('/все-слова'),
        component: 'pages/все-слова.vue'
        //redirect: resolve(__dirname,'static/яроклава-js.яргт')
      });
    }
  }, 

  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    vendor: ['axios'],
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
